package roller

import "testing"

type testRoller struct{}

func (tR testRoller) ParseRoll(string) error {
	return nil
}

func (tR testRoller) Roll() []int {
	return nil
}

func (tR testRoller) PrintRoll() string {
	return ""
}

func TestRegisterNew(t *testing.T) {
	rollersLen := len(Rollers)
	var err error

	t.Log("The Register function should accept a new roller")
	err = Register("test", testRoller{})
	if err != nil {
		t.Error("The roller was not registered")
	}

	t.Log("The Rollers variable should have increased by one")
	newLen := len(Rollers)
	if newLen != rollersLen+1 {
		t.Errorf("%d expected, got %d\n", rollersLen+1, newLen)
	}
}

func TestRegisterDuplicate(t *testing.T) {
	var err error

	rollersLen := len(Rollers)

	t.Log("The Register function should not register duplicate rollers")
	err = Register("test", testRoller{})
	if err == nil {
		t.Error("The duplicate roller was accepted")
	}

	newLen := len(Rollers)
	if newLen != rollersLen {
		t.Errorf("Length of Rollers changed. %d expected, got %d.\n", rollersLen, newLen)
	}
}
