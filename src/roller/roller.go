package roller

import (
	"errors"
	"math/rand"
	"time"
)

var Rollers = make(map[string]roller)

func init() {
	rand.Seed(time.Now().UnixNano())
}

type roller interface {
	ParseRoll(string) error
	Roll() []int
	PrintRoll() string
}

func Register(name string, r roller) error {
	if _, exists := Rollers[name]; exists {
		return errors.New("roller: roller already registered")
	}
	Rollers[name] = r
	return nil
}
