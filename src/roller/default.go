package roller

import (
	"errors"
	"fmt"
	"math/rand"
	"strconv"
	"strings"
)

func init() {
	Register("default", &defaultRoller{})
}

type defaultRoller struct {
	num   int
	sides int
}

func (r *defaultRoller) Roll() []int {
	result := make([]int, r.num)
	for i := 0; i < r.num; i++ {
		result[i] = rand.Intn(r.sides) + 1
	}
	return result
}

func (r *defaultRoller) sum(rolls []int) int {
	total := 0
	for _, roll := range rolls {
		total += roll
	}
	return total
}

func (r *defaultRoller) stringResult(rolls []int) string {
	sRolls := make([]string, len(rolls))
	for i, roll := range rolls {
		sRolls[i] = strconv.Itoa(roll)
	}
	return strings.Join(sRolls[:], ", ")
}

func (r *defaultRoller) PrintRoll() string {
	result := r.Roll()
	return fmt.Sprintf(
		"You rolled %dd%d!\n%s\nTotal Roll: %d",
		r.num,
		r.sides,
		r.stringResult(result),
		r.sum(result),
	)
}

func (r *defaultRoller) ParseRoll(rollString string) error {
	var err error
	splitRoll := strings.Split(rollString, "d")
	if len(splitRoll) != 2 {
		return errors.New("dice: improperly formatted roll")
	} else {
		r.num, err = strconv.Atoi(splitRoll[0])
		if r.num < 1 {
			return errors.New("dice: must have at least one die")
		}
		r.sides, err = strconv.Atoi(splitRoll[1])
		if r.sides < 2 {
			return errors.New("dice: dice must have at least 2 sides")
		}
		return err
	}
}
