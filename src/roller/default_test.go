package roller

import (
	"fmt"
	"regexp"
	"testing"
)

func TestParseRollGoodValues(t *testing.T) {
	dR := &defaultRoller{}
	num := 5
	sides := 7

	t.Log("ParseRoll should accept a string formatted XdY and apply X to the number of dice and Y to the number of sides.")
	err := dR.ParseRoll(fmt.Sprintf("%dd%d", num, sides))

	if err != nil {
		t.Error("Roll string was not correctly parsed.")
	} else {
		if dR.num != num {
			t.Errorf("Incorrect number of dice. %d expected, got %d.\n", num, dR.num)
		}
		if dR.sides != sides {
			t.Errorf("Incorrect number of sides. %d expected, got %d.\n", sides, dR.sides)
		}
	}
}

func TestParseRollBadString(t *testing.T) {
	dR := &defaultRoller{}

	t.Log("ParseRoll should return an error when an incorrect string is entered.")
	err := dR.ParseRoll("4dd6")
	if err == nil {
		t.Error("ParseRoll silently accepted bad roll.")
	}
}

func TestParseRollNoDice(t *testing.T) {
	dR := &defaultRoller{}

	t.Log("ParseRoll should return an error when a roll with zero dice is entered.")
	err := dR.ParseRoll("0d6")
	if err == nil {
		t.Error("ParseRoll silently accepted bad roll.")
	}
}

func TestParseRollNoSides(t *testing.T) {
	dR := &defaultRoller{}

	t.Log("ParseRoll should return an error when a roll with not enough sides.")
	for i := 1; i >= 0; i-- {
		err := dR.ParseRoll(fmt.Sprintf("4d%d", i))
		if err == nil {
			t.Error("ParseRoll silently accepted bad roll.")
		}
	}
}

func TestRoll(t *testing.T) {
	num := 100
	sides := 6
	dR := &defaultRoller{num, sides}
	results := dR.Roll()

	t.Logf("Roll should return a slice with %d values.\n", num)
	rollLen := len(results)
	if rollLen != num {
		t.Errorf("Expected %d values. Got %d.\n", num, rollLen)
	}

	t.Logf("Values from Roll should be between 1 and %d.\n", sides)
	for _, result := range results {
		if result <= 0 || result > sides {
			t.Errorf("Expected value between 1 and %d. Got %d.\n", sides, result)
		}
	}
}

func TestSum(t *testing.T) {
	dR := &defaultRoller{}
	simRoll := []int{1, 2, 3, 4, 5}

	realSum := 0
	for _, n := range simRoll {
		realSum += n
	}

	summedRoll := dR.sum(simRoll)

	t.Logf("sum should return a value of %d.\n", realSum)
	if summedRoll != realSum {
		t.Errorf("sum returned incorrect value. Expected %d, got %d.\n", realSum, summedRoll)
	}
}

func TestStringResult(t *testing.T) {
	dR := &defaultRoller{}
	simRoll := []int{1, 2, 3, 4, 5}
	expectedString := "1, 2, 3, 4, 5"
	actualString := dR.stringResult(simRoll)

	t.Log("stringResult should take a slice of ints, and return a string of comma delimited numbers.")
	if actualString != expectedString {
		t.Errorf("Bad string received. Expected %s, got %s.", expectedString, actualString)
	}
}

func TestPrintRoll(t *testing.T) {
	dR := &defaultRoller{4, 6}
	formattedRoll := dR.PrintRoll()
	r, _ := regexp.Compile("You rolled [1-9]d[1-9]!\n(([1-9], )*[1-9])?\nTotal Roll: [1-9]")
	t.Log("PrintRoll should perform a roll and return it as a formatted string.")
	if !r.MatchString(formattedRoll) {
		t.Errorf("Roll string improperly formatted. Received:\n%s\n", formattedRoll)
	}
}
