package main

import (
	"errors"
	"flag"
	"fmt"
	"log"
	"roller"
)

func main() {
	// declare variables
	var err error
	rollType := "default"
	diceRoller := roller.Rollers[rollType]

	// checks for a roll with the format <x>d<y> and parses it
	// into a number of dice and number of sides
	flag.Parse()
	if len(flag.Args()) > 0 {
		err = diceRoller.ParseRoll(flag.Args()[0])
	} else {
		err = errors.New("dice: no roll entered")
	}

	// checks for errors
	if err != nil {
		// handle error
		log.Fatal(err)
	}
	// creates a new roll, gets results, and prints those to stdout
	myRoll := diceRoller.PrintRoll()
	fmt.Println(myRoll)
}
